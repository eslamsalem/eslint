package convert

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	in := `[
    {
        "filePath": "html/index.html",
        "messages": [
            {
                "endLine": 8,
                "line": 8,
                "message": "Unsafe Regular Expression",
                "ruleId": "security/detect-unsafe-regex",
                "severity": 1,
                "source": "     var emailExpression = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$/;"
            }
        ]
    },
    {
        "filePath": "js/main.js",
        "messages": [
            {
                "endLine": 25,
                "line": 25,
                "message": "Generic Object Injection Sink",
                "ruleId": "security/detect-object-injection",
                "severity": 1,
                "source": ""
            },
            {
                "endLine": 49,
                "line": 47,
                "message": "Potential timing attack, right side: true",
                "ruleId": "security/detect-possible-timing-attacks",
                "severity": 1,
                "source": ""
            },
			{
                "endLine": 55,
                "line": 55,
                "message": "New unknown security rule",
                "ruleId": "security/new-rule",
                "severity": 1,
                "source": ""
            }
        ]
    },
    {
        "filePath": "js/react.jsx",
        "messages": [
            {
                "endLine": 42,
                "line": 42,
                "message": "... componentDidUpdate instead. See https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html.",
                "ruleId": "react/no-unsafe",
                "severity": 1,
                "source": ""
            }
        ]
    }
	]`

	var scanner = issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	r := strings.NewReader(in)
	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{

			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Unsafe Regular Expression",
				Message:     "Unsafe Regular Expression",
				Description: "Potentially unsafe regular expressions. It may take a very long time to run.",
				CompareKey:  "html/index.html:b084550886ce03d51d5213138cb086e85c90cea96b2a6ba2de9453fb75da279e:security/detect-unsafe-regex",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "html/index.html",
					LineStart: 8,
					LineEnd:   8,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-unsafe-regex",
						Value: "security/detect-unsafe-regex",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-unsafe-regex",
					},
				},
				Links: []issue.Link{},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Generic Object Injection Sink",
				Message:     "Generic Object Injection Sink",
				Description: "Bracket object notation with user input is present, this might allow an attacker to access all properties of the object and even it's prototype, leading to possible code execution.",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/detect-object-injection",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "js/main.js",
					LineStart: 25,
					LineEnd:   25,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-object-injection",
						Value: "security/detect-object-injection",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-object-injection",
					},
				},
				Links: []issue.Link{},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "Potential timing attack, right side: true",
				Message:     "Potential timing attack, right side: true",
				Description: "Insecure comparisons (==, !=, !== and ===), which check input sequentially. This could lead to timing attacks on your application.",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/detect-possible-timing-attacks",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "js/main.js",
					LineStart: 47,
					LineEnd:   49,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-possible-timing-attacks",
						Value: "security/detect-possible-timing-attacks",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-possible-timing-attacks",
					},
				},
				Links: []issue.Link{},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "New unknown security rule",
				Message:     "New unknown security rule",
				Description: "New unknown security rule",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/new-rule",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "js/main.js",
					LineStart: 55,
					LineEnd:   55,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/new-rule",
						Value: "security/new-rule",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#new-rule",
					},
				},
				Links: []issue.Link{},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "... componentDidUpdate instead.",
				Message:     "... componentDidUpdate instead.",
				Description: "Detected legacy lifecycle methods that are unsafe for use in async React applications and cause warnings in strict mode",
				CompareKey:  "js/react.jsx:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:react/no-unsafe",
				Severity:    issue.SeverityLevelUnknown,
				Confidence:  issue.ConfidenceLevelUnknown,
				Location: issue.Location{
					File:      "js/react.jsx",
					LineStart: 42,
					LineEnd:   42,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID react/no-unsafe",
						Value: "react/no-unsafe",
						URL:   "https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unsafe.md",
					},
				},
				Links: []issue.Link{
					{
						Name: "",
						URL:  "https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html",
					},
				},
			},
		},
		DependencyFiles: []issue.DependencyFile{},
		Remediations:    []issue.Remediation{},
	}
	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
