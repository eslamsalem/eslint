FROM node:11-alpine

# The node user below doesn't have permission to create a file in /etc/ssl/certs, this
# RUN command creates a file that the analyzer can add additional ca certs to trust.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:node /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

USER node
WORKDIR /home/node

COPY --chown=root:root analyzer /
COPY --chown=node:node eslintrc ./.eslintrc
COPY --chown=node:node babel.config.json .
COPY --chown=node:node package.json .
COPY --chown=node:node yarn.lock .

RUN yarn --frozen-lockfile && yarn cache clean

ENTRYPOINT []
CMD ["/analyzer", "run"]
