# ESLint analyzer changelog

## v2.2.1
- Parse URLs out of messages and add them as Links

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!19)

## v2.1.0
- Add support for custom CA certs (!17)

## v2.0.4
- Use babel-parser to support Stage-0 ES syntax

## v2.0.3
- Add eslint-plugin-react in support of React projects

## v2.0.2
- Update common to v2.1.6

## v2.0.1
- Ignore `.eslintrc` files in the repo

## v2.0.0
- Switch to new report syntax with `version` field

## v1.0.1
- Fix missing `.eslintrc` file when `$HOME` is not set to `/home/node`

## v1.0.0
- Initial release
